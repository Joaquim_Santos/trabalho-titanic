import numpy as np
import pandas as pd
import h2o
from h2o.automl import H2OAutoML

h2o.init(ip="localhost", port=54323)
dados_treino = pd.read_csv('train.csv')
dados_treino.fillna(value=dados_treino['Age'].mean(), inplace=True)
print(dados_treino.columns)

dados_treino = h2o.H2OFrame(dados_treino)
train, test = dados_treino.split_frame(ratios=[.9])

x = ['Sex', 'Age', 'Pclass']
y = 'Survived'

train[y] = train[y].asfactor()
test[y] = test[y].asfactor()

aml = H2OAutoML(max_models = 10, max_runtime_secs=120, seed = 1)
aml.train(x = x, y = y, training_frame = train, leaderboard_frame = test)

#print(aml.leaderboard)
print(aml.leader)

dados_teste = pd.read_csv('test.csv')
dados_teste.fillna(value=dados_teste['Age'].mean(), inplace=True)
resultados_submissao = {}
resultados_submissao['PassengerId'] = dados_teste['PassengerId']

dados_teste = dados_teste[x]
dados_teste = h2o.H2OFrame(dados_teste)
predicoes = aml.leader.predict(dados_teste)

data_frame = predicoes.as_data_frame()
predicoes = data_frame['predict'].tolist()

resultados_submissao['Survived'] = predicoes

df_resultados_submissao = pd.DataFrame(resultados_submissao, columns= ['PassengerId', 'Survived'])
df_resultados_submissao.to_csv('resultados_submissao_automl.csv', index = None, header=True)